include config.mk

$(ODIR)/%.o: $(SDIR)/%.cpp $(DEPS)
	$(CXX) $(CXXFLAGS) -c -o $@ $<

$(NAME): $(OBJ)
	$(CXX) $(CXXFLAGS) -o $@ $^ $(LIB)

obj/number_systems.so: src/number_systems.cpp $(DEPS)
	$(CXX) $(CXXFLAGS) -c -fPIC -o obj/number_systems.so src/number_systems.cpp

libnumber_systems.a: obj/number_systems.o
	$(AR) $(ARFLAGS) libnumber_systems.a obj/number_systems.o

libnumber_systems.so: obj/number_systems.so
	$(CXX) $(CXXFLAGS) -shared -o libnumber_systems.so obj/number_systems.so

.PHONY: all clean test

all: $(NAME)

clean:
	rm -f $(NAME) test $(ODIR)/*.o $(ODIR)/*.so \
	      libnumber_systems.a libnumber_systems.so

test: src/test.cpp obj/number_systems.o
	$(CXX) $(CXXFLAGS_T) -c -o obj/test.o src/test.cpp $(LIB) 
	$(CXX) $(CXXFLAGS_T) -o test obj/test.o obj/number_systems.o $(LIB)
	./test | column -t

