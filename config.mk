AR  = ar
CXX = g++

ARFLAGS    = rcs
CXXFLAGS   = -Wall -I $(IDIR)
CXXFLAGS_T = -Wall -I $(IDIR) -D ADDITIVE -D POSITIONAL -D BALANCED
#CXXFLAGS_T = -Wall -I $(IDIR) -D BALANCED -D VERBOSE

ifdef DEBUG
CXXFLAGS   += -g
CXXFLAGS_T += -g
else
CXXFLAGS += -D NDEBUG
endif

LIB = -lgmp -lgmpxx

IDIR  = include
ODIR  = obj
SDIR  = src

_DEPS = number_systems.hpp
_OBJ  = number_systems.o main.o

DEPS = $(patsubst %,$(IDIR)/%,$(_DEPS))
OBJ  = $(patsubst %,$(ODIR)/%,$(_OBJ))

NAME = cnv

