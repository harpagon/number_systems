/*
 * Copyright (C) 2019  Michał KLoc <michal.kloc@yahoo.com>
 *
 * This file is part of number_systems library.
 *
 * Number_systems is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef __NUMBER_SYSTEMS_HPP__
#define __NUMBER_SYSTEMS_HPP__

#include <string>

#include <gmp.h>
#include <gmpxx.h>

using std::string;

typedef string (*to_mpz_converter)(mpz_class, unsigned int);
typedef mpz_class (*from_mpz_converter)(string, unsigned int);

string to_regular  (mpz_class number, unsigned int base=10);
string to_bijective(mpz_class number, unsigned int base=10);
string to_factorial(mpz_class number, unsigned int _=0);
string to_bter     (mpz_class number, unsigned int _=0);
string to_balanced (mpz_class number, unsigned int base=10);
string to_additive (mpz_class number, unsigned int base=10);
string to_roman    (mpz_class number, unsigned int _=0);
string to_unary    (mpz_class number, unsigned int _=0);

mpz_class from_const_pos(string str, unsigned int base=10);
mpz_class from_factorial(string str, unsigned int _=0);
mpz_class from_bter     (string str, unsigned int _=0);
mpz_class from_balanced (string str, unsigned int base=10);
mpz_class from_additive (string str, unsigned int base=10);
mpz_class from_roman    (string str, unsigned int _=0);
mpz_class from_unary    (string str, unsigned int _=0);

#endif//__NUMBER_SYSTEMS_HPP__

