/*
 * Copyright (C) 2019  Michał KLoc <michal.kloc@yahoo.com>
 *
 * This file is part of number_systems library.
 *
 * Number_systems is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <string>
#include <iostream>

#include <gmp.h>
#include <gmpxx.h>

#include "number_systems.hpp"

using std::string;

#define ONE '1'

static char
encode
(unsigned int digit)
{
	char c = static_cast<char>(digit);
	if(digit < 10) return c + '0';
	if(digit < 36) return c + 'A' - 10;
	if(digit < 62) return c + 'a' - 36;
	return 0; // throw ?
}

static unsigned int
decode
(char c)
{
	if('0' <= c && c <= '9') return c - '0';
	if('A' <= c && c <= 'Z') return c - 'A' + 10;
	if('a' <= c && c <= 'z') return c - 'a' + 36;
	return -1; // throw ?
}

string
to_regular
(mpz_class number, unsigned int base)
{
	//return mpz_get_str(NULL, base, number.get_mpz_t());

	string result;
	mpz_class exponent = 1;
	unsigned int digit;

	do {
		mpz_class tmp = number % base;
		digit = tmp.get_ui();
		result = encode(digit) + result;
		number /= base;
	} while(number != 0);

	return result;
}

string
to_bijective
(mpz_class number, unsigned int base)
{
	string result;
	unsigned int digit;

	while(number != 0) {
		mpz_class tmp = number % base;
		digit = mpz_get_ui(tmp.get_mpz_t());
		if(digit) {
			number /= base;
		} else {
			number /= base;
			number -= 1;
			digit = base;
		}
		result = encode(digit) + result;
	}

	return result;
}

string
to_factorial
(mpz_class number, unsigned int _)
{
	if(number == 0) return "0";

	string result;
	unsigned int digit;
	mpz_class i = 1, exponent = 1, tmp;

	// find the highest digit
	while(number >= exponent) {
		i += 1;
		exponent *= i;
	}
	exponent /= i;
	i -= 1;

	while(i > 0) {
		tmp       = number / exponent;
		digit     = mpz_get_ui(tmp.get_mpz_t());
		result   += encode(digit);
		number   -= exponent * digit;
		exponent /= i;
		i        -= 1;
	}

	return result;
}

string
to_bter
(mpz_class number, unsigned int _)
{
	string result;

	do {
		mpz_class tmp_mpz = number % 3;
		int tmp = tmp_mpz.get_si();
		if(tmp == 0) {
			result = 'o' + result;
		} else if(tmp == 1 or tmp == -2) {
			result = '+' + result;
			number -= 1;
		} else {
			result = '-' + result;
			number += 1;
		}
		number /= 3;
	} while(number != 0);

	return result;
}

string
to_balanced
(mpz_class number, unsigned int base)
{
	string result;
	mpz_class exponent = 1;

	int
		digit_val,
		is_negative = number < 0,
		max_digit_val = is_negative ? (base / 2 - !(base % 2)) : (base / 2),
		carry = 0;

	if(is_negative) number = -number;

	do {
		mpz_class tmp = (number % base);
		digit_val = tmp.get_si();
		if(digit_val > max_digit_val) {
			digit_val = base - digit_val;
			carry = 1;
		}
		result = encode(digit_val) + result;
		if((digit_val and !carry and is_negative) or (carry and !is_negative)) {
			result = '`' + result;
		}
		number /= base;
		number += carry;
		carry = 0;
	} while(number != 0);

	return result;
}

string
to_additive
(mpz_class number, unsigned int base)
{
	if(base < 2) return to_bijective(number, 1);

	string result;
	unsigned int i = 0;
	mpz_class multiplier = 1;

	// find the highest digit
	while(number >= multiplier) {
		multiplier *= base;
		i += 1;
	}
	multiplier /= base;

	while(number!=0) {
		while(number >= multiplier) {
			number -= multiplier;
			result += encode(i);
		}
		multiplier /= base;
		i -= 1;
	}

	return result;
}

string
to_unary
(mpz_class number, unsigned int _)
{
	string result;

	while(number > 0) {
		unsigned int i = 4;
		if(number > 4) {
			result += "-";
			while(i) {
				result += "I";
				number -= 1;
				i -= 1;
			}
			result+="- ";
			number -= 1;
		} else {
			result+="I";
			number -= 1;
		}
	}

	return result;
}

string
to_roman
(mpz_class number, unsigned int _)
{
	string result;
	unsigned int i = 0, val;

	const char
		*digits[] = {
			"M", "CM", "D", "CD",
			"C", "XC", "L", "XL",
			"X", "IX", "V", "IV",
			"I", ""              },
		*current_digit;

	const int
		values[] = {
			1000, 900, 500, 400,
			 100,  90,  50,  40,
			  10,   9,   5,   4,
			   1,   0           };

#define ROMAN_DIGIT_COUNT sizeof(digits)

	// find highest digit
	while(i < ROMAN_DIGIT_COUNT) {
		val = values[i];
		if(val <= number) break;
		i += 1;
	}

	current_digit = digits[i];

	while(number) {
		while(val <= number) {
			result += current_digit;
			number -= val;
		}
		i += 1;
		val = values[i];
		current_digit = digits[i];
	}

	return result;
}

mpz_class
from_const_pos
(const string str, unsigned int base)
{
	mpz_class result = 0, exponent = 1;
	for(int i = str.size() - 1; i >= 0; --i) {
		result += exponent * decode(str[i]);
		exponent *= base;
	}
	return result;
}

mpz_class
from_factorial
(const string str, unsigned int _)
{
	mpz_class result = 0, factorial = 1;
	int n = str.size() + 1;
	for(int i = str.size() - 1; i >= 0; --i) {
		result += factorial * decode(str[i]);
		factorial *= n - i;
	}
	return result;
}

mpz_class
from_bter
(const string str, unsigned int _)
{
	mpz_class result = 0, exponent = 1;

	for(int i = str.size() - 1; i >= 0; --i) {
		switch(str[i]) {
		case '+': result += exponent; break;
		case '-': result -= exponent; break;
		case 'o': break;
		default : return -1;
		}
		exponent *= 3;
	}
	return result;
}

mpz_class
from_balanced
(const string str, unsigned int base)
{
	mpz_class result = 0, exponent = 1;
	int i = str.size() - 1;
	while(i > 0) {
		mpz_class tmp = exponent * decode(str[i--]);
		if(str[i] == '`') {
			result -= tmp;
			i -= 1;
		} else {
			result += tmp;
		}
		exponent *= base;
	}

	if(str[0] != '`') {
		mpz_class tmp = exponent * decode(str[0]);
		result += tmp;
	};

	return result;
}

mpz_class
from_additive
(const string str, unsigned int base)
{
	mpz_class result = 0;
	int str_size = str.size(), multiplier = 1;
	unsigned int largest_value = decode(ONE);

	// find largest digit
	for(int s = 0; s < str_size; ++s) {
		unsigned int v = decode(str[s]);
		if(v > largest_value) largest_value = v;
	}

	for(unsigned int v = 1; v <= largest_value; ++v) {
		char c = encode(v);
		int count = 0;
		for(int s = 0; s < str_size; ++s) {
			if(str[s] == c) count += 1;
		}
		result += count * multiplier;
		multiplier *= base;
	}

	return result;
}

mpz_class
from_roman
(const string str, unsigned int _)
{
	mpz_class result = 0;
	int s = 0, str_size = str.size();

	while(s < str_size) {
		char tmp = str[s++];
		switch(tmp) {
		case 'M': result += 1000; break;
		case 'D': result +=  500; break;
		case 'L': result +=   50; break;
		case 'V': result +=    5; break;

#define roman_case(c, n1, n2, v1, v2, ve)               \
		case (c): {                                     \
			char next = str[s++];                       \
			if     (next == (n1)) result += (v1);       \
			else if(next == (n2)) result += (v2);       \
			else                 {result += (ve); --s;} \
			break;                                      \
		}                                               \

		roman_case('C', 'M', 'D', 900, 400, 100);
		roman_case('X', 'C', 'L',  90,  40,  10);
		roman_case('I', 'X', 'V',   9,   4,   1);
#undef roman_case

		default: return -1;
		}
	}

	return result;
}

mpz_class
from_unary
(const string str, unsigned int _)
{
	mpz_class result = 0;
	int s = 0, str_size = str.size();

	while(s < str_size) {
		switch(str[s]) {
		case '-': result += 5; s += 7; continue;
		case 'I': result += 1; s += 1; continue;
		case ' ':              s += 1; continue;
		default : return -1;
		}
	}

	return result;
}

