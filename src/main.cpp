/*
 * cnv - convert a number in regular positional system into another.
 * Copyright (C) 2019  Michał KLoc <michal.kloc@yahoo.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <iostream>

#include <getopt.h>
#include <gmp.h>
#include <gmpxx.h>

#include "number_systems.hpp"

static void
help
(const char *name)
{
	std::cerr
		<< '\n'
		<< "Encode given number in regular positional numberal system into"
		   " another system.\n"
		<< '\n'
		<< "\tAvaiable conversion functions:\n"
		<< "\t\t- " << "additive" << "    (a)" << '\n'
		<< "\t\t- " << "balanced" << "    (c)" << '\n'
		<< "\t\t- " << "bijective" << "   (b)" << '\n'
		<< "\t\t- " << "factorial" << "   (f)" << " - ignores base" << '\n'
		<< "\t\t- " << "regular" << "     (r)" << " - standard" << '\n'
		<< "\t\t- " << "roman" << "       (m)" << " - ignores base" << '\n'
		<< "\t\t- " << "ternary-bal" << " (t)" << " - ignores base" << '\n'
		<< "\t\t- " << "unary" << "       (u)" << " - ignores base" << '\n'
		<< '\n'
		<< "\tOther options:\n"
		<< "\t\t- " << "help" << "        (h)" << '\n'
		<< "\t\t- " << "input_base" << "  (i)" << " {IBASE}" << '\n'
		<< "\t\t- " << "output_base" << " (o)" << " {OBASE}" << '\n'
		<< "\t\t- " << "separator" << "   (s)" << " {SEP}" << '\n'
		<< "\t\t- " << "varbose" << "     (v)" << " [SEP]" << '\n'
		<< '\n'
		<< "Pass output method using above keywords as long options to\n"
		   "the program (short option versions listed in parentheses).\n"
		<< '\n'
		<< "Numbers >= 0 are supported in this program.\n"
		<< "Ternary-bal function supports additionally negative numbers.\n"
		<< '\n'
		<< "\tExample usage:\n"
		<< "\t\t" << name << " --" << "bijective" << " 10 20 30 100\n"
		<< "\t\t" << name << " --" << "bijective" << " -i6" << " -o3" << " 10 20 30 100\n"
		<< "\t\t" << name << " --" << "factorial" << " 10 20 30 100\n"
		<< "\t\t" << name << " --" << "verbose="  << "\" --> \" -o 7" << " 10 20 30 100\n"
		<< std::endl;

	exit(1);
}

int
main
(int argc, char **argv)
{
	int
		input_option = 0,
		option_index = 0,
		verbose      = 0;

	to_mpz_converter
		converter = to_regular;

	mpz_class
		ibase = 10,
		input = 0,
		obase = 10;

	const char
		*sep = "\n",
		*vsep = " ";

	struct option long_options[] = {
		{"additive",    no_argument,       0,             'a'},
		{"balanced",    no_argument,       0,             'c'},
		{"bijective",   no_argument,       0,             'b'},
		{"factorial",   no_argument,       0,             'f'},
		{"help",        no_argument,       0,             'h'},
		{"input_base",  required_argument, &input_option, 'i'},
		{"output_base", required_argument, &input_option, 'o'},
		{"regular",     no_argument,       0,             'r'},
		{"roman",       no_argument,       0,             'm'},
		{"separator",   required_argument, &input_option, 's'},
		{"ternary-bal", no_argument,       0,             't'},
		{"unary",       no_argument,       0,             'u'},
		{"verbose",     optional_argument, &input_option, 'v'},
		{0, 0, 0, 0},
	};

opt_parser:
#define OPTS "abcfhi:o:rs:tuv" 
	switch(getopt_long(argc, argv, OPTS, long_options, &option_index)) {

	case -1: goto program;

	case  0:
		switch(input_option) {
		case 1  :
		case 'i': mpz_set_str(ibase.get_mpz_t(), optarg, 10); break;
		case 'o': mpz_set_str(obase.get_mpz_t(), optarg, 10); break;
		case 's': sep = optarg;                               break;
		case 'v': verbose = 1; if(optarg) vsep = optarg;      break;
		default : help(argv[0]);
		}
		break;

	case 'i': mpz_set_str(ibase.get_mpz_t(), optarg, 10); break;
	case 'o': mpz_set_str(obase.get_mpz_t(), optarg, 10); break;

	case 'a': converter = to_additive;  break;
	case 'b': converter = to_bijective; break;
	case 'c': converter = to_balanced;  break;
	case 'f': converter = to_factorial; break;
	case 'm': converter = to_roman;     break;
	case 'r': converter = to_regular;   break;
	case 't': converter = to_bter;      break;
	case 'u': converter = to_unary;     break;

	case 's': sep = optarg; break;
	case 'v': verbose = 1; if(optarg) vsep = optarg; break;

	default : help(argv[0]);
	}
	goto opt_parser;

program:
	while(optind < argc) {
		const char *curr = argv[optind++];
		mpz_set_str(input.get_mpz_t(), curr, ibase.get_ui());
		if(verbose) std::cout << curr << vsep;
		std::cout << converter(input, obase.get_ui()) << sep;
	}

	return 0;
}

