/*
 * Copyright (C) 2019  Michał KLoc <michal.kloc@yahoo.com>
 *
 * Number_systems is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <cassert>
#include <iostream>
#include <string>

#include "number_systems.hpp"

using std::endl;
using std::cerr;
using std::cout;
using std::string;

const char *SEP = "\t";

int
main
(int argc, char **argv)
{
#ifdef ADDITIVE
	#ifdef VERBOSE
	cout <<
		"dec(0-9)\tunary\tfrom\tbinary\tfrom\tternary\tfrom\t"
		"septenary\tfrom\tdecimal\tfrom\tsexagesimal\tfrom\troman\tfrom\n";
	#endif
	for(int i = 0; i < 61; ++i) {
		string
			unary_      = to_unary(i),
			unary       = to_additive(i, 1),
			binary      = to_additive(i, 2),
			ternary     = to_additive(i, 3),
			septenary   = to_additive(i, 7),
			decimal     = to_additive(i, 10),
			sexagesimal = to_additive(i, 60),
			roman       = to_roman(i);

		mpz_class
			r_unary_      = from_unary(unary_),
			r_unary       = from_additive(unary, 1),
			r_binary      = from_additive(binary, 2),
			r_ternary     = from_additive(ternary, 3),
			r_septenary   = from_additive(septenary, 7),
			r_decimal     = from_additive(decimal, 10),
			r_sexagesimal = from_additive(sexagesimal, 60),
			r_roman       = from_roman(roman);

		assert(r_unary_      == i);
		assert(r_unary       == i);
		assert(r_binary      == i);
		assert(r_ternary     == i);
		assert(r_septenary   == i);
		assert(r_decimal     == i);
		assert(r_sexagesimal == i);
		assert(r_roman       == i);

	#ifdef VERBOSE
		cout
			<< i           << SEP
			<< unary       << SEP << r_unary       << SEP
			<< binary      << SEP << r_binary      << SEP
			<< ternary     << SEP << r_ternary     << SEP
			<< septenary   << SEP << r_septenary   << SEP
			<< decimal     << SEP << r_decimal     << SEP
			<< sexagesimal << SEP << r_sexagesimal << SEP
			<< roman       << SEP << r_roman       << SEP
			<< '\n';
	#endif
	}
	cerr << "\x1B[92mADDITIVE test passed.\x1B[39m" << endl;
#endif

#ifdef POSITIONAL
	#ifdef VERBOSE
	cout <<
		"dec\tfactorial"
		"\trbinary\tbbinary\trternary\tbternary\t"
		"rseptenary\tbseptenary\trdecimal\tbdecimal\t"
		"rsexagesimal\tbsexagesimal\n";
	#endif
	for(int i = 0; i < 61; ++i) {
		string
			factorial   = to_factorial(i),
			binary      = to_regular  (i,  2),
			binary_     = to_bijective(i,  2),
			ternary     = to_regular  (i,  3),
			ternary_    = to_bijective(i,  3),
			septenary   = to_regular  (i,  7),
			septenary_  = to_bijective(i,  7),
			decimal     = to_regular  (i, 10),
			decimal_    = to_bijective(i, 10),
			sexagesimal = to_regular  (i, 60),
			sexagesimal_= to_bijective(i, 60);

		mpz_class
			r_factorial    = from_factorial(factorial),
			r_binary       = from_const_pos(binary,        2),
			r_binary_      = from_const_pos(binary_,       2),
			r_ternary      = from_const_pos(ternary,       3),
			r_ternary_     = from_const_pos(ternary_,      3),
			r_septenary    = from_const_pos(septenary,     7),
			r_septenary_   = from_const_pos(septenary_,    7),
			r_decimal      = from_const_pos(decimal,      10),
			r_decimal_     = from_const_pos(decimal_,     10),
			r_sexagesimal  = from_const_pos(sexagesimal,  60),
			r_sexagesimal_ = from_const_pos(sexagesimal_, 60);

		assert(r_factorial    == i);
		assert(r_binary       == i);
		assert(r_binary_      == i);
		assert(r_ternary      == i);
		assert(r_ternary_     == i);
		assert(r_septenary    == i);
		assert(r_septenary_   == i);
		assert(r_decimal      == i);
		assert(r_decimal_     == i);
		assert(r_sexagesimal  == i);
		assert(r_sexagesimal_ == i);

	#ifdef VERBOSE
		cout
			<< i            << SEP
			<< factorial    << SEP
			<< binary       << SEP
			<< binary_      << SEP
			<< ternary      << SEP
			<< ternary_     << SEP
			<< septenary    << SEP
			<< septenary_   << SEP
			<< decimal      << SEP
			<< decimal_     << SEP
			<< sexagesimal  << SEP
			<< sexagesimal_ << SEP
			<< '\n';
	#endif
	}
	cerr << "\x1B[92mPOSITIONAL test passed.\x1B[39m" << endl;
#endif

#ifdef BALANCED
	#ifdef VERBOSE
	cout <<
		"dec(0-9)\tternary(-/+)\tfrom\tternary(`1-1)\tfrom\t"
		"septenary(`3-3)\tfrom\tdecimal(`4-5)\tfrom\t"
		"centevigesimal(`59-60)\tfrom\n";
	#endif
	for(int i = -200; i < 200; ++i) {
		string
			ternary_       = to_bter(i),
			ternary        = to_balanced(i, 3),
			septenary      = to_balanced(i, 7),
			decimal        = to_balanced(i, 10),
			centevigesimal = to_balanced(i, 120);

		mpz_class
			r_ternary_       = from_bter    (ternary_),
			r_ternary        = from_balanced(ternary, 3),
			r_septenary      = from_balanced(septenary, 7),
			r_decimal        = from_balanced(decimal, 10),
			r_centevigesimal = from_balanced(centevigesimal, 120);

		assert(r_ternary_       == i);
		assert(r_ternary        == i);
		assert(r_septenary      == i);
		assert(r_decimal        == i);
		assert(r_centevigesimal == i);

	#ifdef VERBOSE
		cout
			<< i              << SEP
			<< ternary_       << SEP << r_ternary_       << SEP
			<< ternary        << SEP << r_ternary        << SEP
			<< septenary      << SEP << r_septenary      << SEP
			<< decimal        << SEP << r_decimal        << SEP
			<< centevigesimal << SEP << r_centevigesimal << SEP
			<< '\n';
	#endif
	}
	cerr << "\x1B[92mBALANCED test passed.\x1B[39m" << endl;
#endif
}

