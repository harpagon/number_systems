#!/bin/bash

echo

case "$1" in

all_of_type)
	echo -e "All the nubmers 0-62 of type $2\n"
	if [ -z "$2" ]; then
		echo -e "Provide TYPE.\n"
	else
		case "$2" in
		factorial|ternary-bal|roman|unary)
			for i in $(seq 0 62); do
				./cnv --$2 $i
			done
			;;
		additive|bijective|regular)
			for i in $(seq 0 62); do
				for j in $(seq 2 62); do
					#./cnv -o $j $i | tr -d '\n'
					./cnv --$2 -s" " -o $j $i
					echo -n ' '
				done
				echo
			done
			;;
		*)
			echo "You prat (no such a type).\n"
		esac
	fi
	;;

decimal_comparison)
	echo -e "Decimal comparison\n"

	BASE_TYPES="regular bijective factorial ternary-bal"
	NUMBERS="$(seq 1 200)
		2432902008176640000 
		1124000727777607680000
		166589903787325219380851695350896256250980509594874862046961683989710"
	for bdesc in $BASE_TYPES; do
		echo -n "$bdesc "
	done
	echo
	for i in $NUMBERS; do
		for b in $BASE_TYPES; do
			./cnv -s" " --$b $i
			echo -n ' '
		done
		echo
	done
	;;

decimals_additive)
	echo -e "Additive system - decimals\n"
	./cnv --verbose --additive $(seq 0 100)
	;;

binary_additive)
	echo -e "Additive system - binary\n"
	./cnv --verbose --additive -o 2 $(seq 0 20)
	;;

unary)
	echo -e "Unary\n"
	./cnv --verbose="    " --unary $(seq 0 20)
	;;

generic)
	echo -e "Generic\n"
	if [ -z "$2" ] || [ -z "$3" ] || [ -z $4 ]; then
		echo -e "Provide TYPE, BASE, NUMBER_COUNT.\n"
	else
		i=0
		while [ $i -le $4 ]; do
			echo -n "$i "
			./cnv --$2 -o$3 $i
			let i+=1
		done
	fi
	;;

*)
	echo -e \
		"SYNTAX:\n"\
		"\t$0 {ACTION}\n"\
		"\n"\
		"Actions are:\n"\
		"\tall_of_type\n"\
		"\tdecimal_comparison\n"\
		"\tdecimals_additive\n"\
		"\tbinary_additive\n"\
		"\tunary\n"\
		"\tgeneric {TYPE} {BASE} {NUMBER_COUNT}\n"\
		"\n"\
		"\tSee also: ./cnv --help\n"
	;;

esac

