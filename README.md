# Intro

This is rewritten version of cnv program I created about 4 years ago.

# Building

Project requires __gmp__ library.

Build the project:
```
make
```

Build static library:
```
make libnumber_systems.a
```

Build shared library:
```
make libnumber_systems.so
```

Test (build and run):
```
make test
```
(See test section for more information)

Run examples.sh for some example examples.

# cnv

Converts list of numbers in regular positional system into another one.

## Available conversion functions

* additive `(-a, --additive)`
* balanced `(-c, --balanced)`
* bijective `(-b, --bijective)`
* factorial `(-f, --factorial)`
* regular `(-r, --regular)`
* roman `(-m, --roman)`
* ternary balanced `(-t, --ternary-bal)`
* unary `(-u, --unary)`

## Additional options:

* help `(-h, --help)`
* input base `(-i {IBASE}, --input_base={IBASE})`
* output base `(-o {OBASE}, --output_base={OBASE})`
* separator `(-s {SEP}, --separator={SEP})` - set separator between each output
number
* verbose `(-v [SEP], --verbose[=SEP])` - for each ouput number print the input
it was based on. Optionally separator can be given.

# number\_systems

This library is based on __gmp__ library.

Each function in this library fall into one of two categories:

* encoders (convert from `mpz_class` into `std::string`)
* decoders (convert from `std::string` into `mpz_class`)

For each function category there is helper type declared:

```C++
typedef string (*to_mpz_converter)(mpz_class, unsigned int);    // encoders
typedef mpz_class (*from_mpz_converter)(string, unsigned int);  // decoders
```

Encoders `to_bter`, `to_balanced` can handle negative numbers.
Passing a negative number to any other encoder won't give correct result.

Encoders use characters (__0-9__ __A-Z__ __a-z__) as digits.

Exceptions:

* balanced extends above set with negated digits (using backtick. Eg: \`1, \`2, \`A etc.)
* balanced ternary uses characters (__- 0 +__)
* roman system uses characters (__I V X L C D M__)
* unary system uses strings __I__ and __-IIII-__.

The same rules apply to decoders.

| name              | encoder        | decoder          | base    | 
| ----------------- | -------------- | ---------------- |:-------:|
| standard          | `to_regular`   | `from_const_pos` | 2 - 62  |
| bijective         | `to_bijective` | `from_const_pos` | 1 - 61  |
| balanced          | `to_balanced`  | `from_balanced`  | 2 - 124 |
| balanced ternary  | `to_bter`      | `from_bter`      | ignores |
| factorial         | `to_factorial` | `from_factorial` | ignores |
| additive          | `to_additive`  | `from_additive`  | any     |
| roman             | `to_roman`     | `from_roman`     | ignores |
| unary             | `to_unary`     | `from_unary`     | ignores |

`to_roman` and `from_roman` use subtractive notation (IV = 4, IX, XL, XC, CL, CM = 900).

# Test

Test is divided into sections:

* ADDITIVE - additive, roman and unary functions (numbers 0 - 61, bases 1, 2, 3, 7, 10, 60)
* POSITIONAL - factorial, standard, bijective (numbers 0 - 61, bases 2, 3, 7, 10, 60)
* BALANCED - balanced ternary and generic balanced (numbers -200 - 200, bases 3, 7, 10, 120)

Setting ADDITIVE, POSITIONAL, BALANCED preprocessor variables activates adequate test.
Set VERBOSE variable to print out tested numbers and their representations in various numeral systems.

